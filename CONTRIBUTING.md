# Contributing

When contributing to this repository, please first discuss the change you wish to make via [issue](https://gitlab.com/pamapam/frontend/issues) before making a change. 

Take a look Readme in order to create your development environment

Go to the repository directory and start coding :) 

## Merge Request Process

We work with **gitflow workflow** [read more about this process](https://es.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

When your contribution is ready `git push` and open [Merge Request on Gitlab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html), always describe your proposal and what issue closes.

The repository maintainers will review your code and in the Merge Request they can request changes or merge them. 