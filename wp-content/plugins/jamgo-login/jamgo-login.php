<?php

global $baseBackUrl;
global $baseBackInternalUrl;

// start the user session for maintaining individual user states during the multi-stage authentication flow:
session_start();

// DEFINE THE OAUTH PROVIDER AND SETTINGS TO USE #
$_SESSION ['WPOA'] ['PROVIDER'] = 'Jamgo';
define('CLIENT_ID', "pamapam-site");
define('CLIENT_SECRET', "secret");
define('REDIRECT_URI', rtrim(get_site_url(), '/') . '/');
define('SCOPE', 'read'); // PROVIDER SPECIFIC: 'read' is the minimum scope required to get the user's id from Jamgo

define('URL_AUTH', $baseBackUrl . "/oauth/authorize?");
define('URL_TOKEN', $baseBackInternalUrl . "/oauth/token?");
define('URL_USER', $baseBackInternalUrl . "/api/user/");

define('FRONT_COOKIE', "wordpress_origin");
// END OF DEFINE THE OAUTH PROVIDER AND SETTINGS TO USE #

# AUTHENTICATION FLOW #
// the oauth 2.0 authentication flow will start in this script and make several calls to the third-party authentication provider which in turn will make callbacks to this script that we continue to handle until the login completes with a success or failure:
if (!CLIENT_ID || !CLIENT_SECRET) {
	// do not proceed if id or secret is null:
	wpoa_end_login("This third-party authentication provider has not been configured with an API key/secret. Please notify the admin or try again later.");
}
elseif (isset($_GET['error_description'])) {
	// do not proceed if an error was detected:
	wpoa_end_login($_GET['error_description']);
}
elseif (isset($_GET['error_message'])) {
	// do not proceed if an error was detected:
	wpoa_end_login($_GET['error_message']);
}
elseif (isset($_GET['code'])) {
	// post-auth phase, verify the state:
	if ($_SESSION['WPOA']['STATE'] == $_GET['state']) {
		// get an access token from the third party provider:
		get_oauth_token();
		// get the user's third-party identity and attempt to login/register a matching wordpress user account:
		$oauth_identity = get_oauth_identity();
		wpoa_login_user($oauth_identity);
	}
	else {
		// possible CSRF attack, end the login with a generic message to the user and a detailed message to the admin/logs in case of abuse:
		// TODO: report detailed message to admin/logs here...
		wpoa_end_login("Sorry, we couldn't log you in. Please notify the admin or try again later.");
	}
}
else {
	// pre-auth, start the auth process:
	if ((empty($_SESSION['WPOA']['EXPIRES_AT'])) || (time() > $_SESSION['WPOA']['EXPIRES_AT'])) {
		// expired token; clear the state:
		wpoa_clear_login_state();
	}
	get_oauth_code();
}

function get_oauth_code() {
	$params = array(
			'response_type' => 'code',
			'client_id' => CLIENT_ID,
			'scope' => SCOPE,
			'state' => uniqid('', true),
			'redirect_uri' => REDIRECT_URI,
	);
	$_SESSION['WPOA']['STATE'] = $params['state'];
	$url = URL_AUTH . http_build_query($params);
	setcookie(FRONT_COOKIE, "true", null, '/');
	header("Location: $url");
	exit;
}

function get_oauth_token() {
	$params = array(
			'grant_type' => 'authorization_code',
			'client_id' => CLIENT_ID,
			'client_secret' => CLIENT_SECRET,
			'code' => $_GET['code'],
			'redirect_uri' => REDIRECT_URI,
	);

	$url_params = http_build_query($params);

	$url = URL_TOKEN . $url_params;
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_USERPWD, CLIENT_ID . ":" . CLIENT_SECRET);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, (get_option('wpoa_http_util_verify_ssl') == 1 ? 1 : 0));
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, (get_option('wpoa_http_util_verify_ssl') == 1 ? 2 : 0));
	$result = curl_exec($curl);

	// parse the result:
	$result_obj = json_decode($result, true); // PROVIDER SPECIFIC: Jamgo encodes the access token result as json by default
	$access_token = $result_obj ['access_token']; // PROVIDER SPECIFIC: this is how Jamgo returns the access token KEEP THIS PROTECTED!
	$expires_in = $result_obj ['expires_in']; // PROVIDER SPECIFIC: this is how Jamgo returns the access token's expiration
	$expires_at = time() + $expires_in;
	// handle the result:
	if (! $access_token || ! $expires_in) {
		// malformed access token result detected:
		wpoa_end_login("Sorry, we couldn't log you in. Malformed access token result detected. Please notify the admin or try again later.");
	} else {
		$_SESSION ['WPOA'] ['ACCESS_TOKEN'] = $access_token;
		$_SESSION ['WPOA'] ['EXPIRES_IN'] = $expires_in;
		$_SESSION ['WPOA'] ['EXPIRES_AT'] = $expires_at;
		return true;
	}
}

function get_oauth_identity() {
	// here we exchange the access token for the user info...
	// set the access token param:
	$params = array (
			'access_token' => $_SESSION ['WPOA'] ['ACCESS_TOKEN']  // PROVIDER SPECIFIC: the access token is passed to Jamgo using this key name
	);
	$url_params = http_build_query($params);
	// perform the http request:
	$url = rtrim(URL_USER, "?");
	$opts = array (
			'http' => array (
					'method' => 'GET',
					'header' => "Authorization: Bearer " . $_SESSION ['WPOA'] ['ACCESS_TOKEN'] . "\r\n" . "x-li-format: json\r\n" . // PROVIDER SPECIFIC: i think only LinkedIn uses x-li-format...
					"Cookie: JSESSIONID=" . $_COOKIE['JSESSIONID'] . "\r\n"
			)
	);
	$context = $context = stream_context_create($opts);
	$result = @file_get_contents($url, false, $context);
	if ($result === false) {
		wpoa_end_login("Sorry, we couldn't log you in. Could not retrieve user identity via stream context. Please notify the admin or try again later.");
	}
	$result_obj = json_decode($result, true);
	// parse and return the user's oauth identity:
	$oauth_identity = array ();
	$oauth_identity ['provider'] = $_SESSION ['WPOA'] ['PROVIDER'];
	$oauth_identity ['id'] = $result_obj ['id']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	$oauth_identity ['username'] = $result_obj ['username']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	$oauth_identity ['name'] = $result_obj ['name']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	$oauth_identity ['surname'] = $result_obj ['surname']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	$oauth_identity ['email'] = $result_obj ['email']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	$oauth_identity ['roleNames'] = $result_obj ['roleNames']; // PROVIDER SPECIFIC: this is how Jamgo returns the user's unique id
	// $oauth_identity['email'] = $result_obj['email']; //PROVIDER SPECIFIC: this is how Jamgo returns the email address
	if (! $oauth_identity ['id']) {
		wpoa_end_login("Sorry, we couldn't log you in. User identity was not found. Please notify the admin or try again later.");
	}
	return $oauth_identity;
}

// ends the login request by clearing the login state and redirecting the user to the desired page:
function wpoa_end_login($msg) {
	$last_url = $_SESSION ["WPOA"] ["LAST_URL"];
	unset($_SESSION ["WPOA"] ["LAST_URL"]);
	$_SESSION ["WPOA"] ["RESULT"] = $msg;
	wpoa_clear_login_state();
//	$redirect_method = get_option("wpoa_login_redirect");
	$redirect_method = "home_page";
	$redirect_url = "";
	switch ($redirect_method) {
		case "home_page" :
			$redirect_url = site_url();
			break;
		case "last_page" :
			$redirect_url = $last_url;
			break;
		case "specific_page" :
			$redirect_url = get_permalink(get_option('wpoa_login_redirect_page'));
			break;
		case "admin_dashboard" :
			$redirect_url = admin_url();
			break;
		case "user_profile" :
			$redirect_url = get_edit_user_link();
			break;
		case "custom_url" :
			$redirect_url = get_option('wpoa_login_redirect_url');
			break;
	}
	// header("Location: " . $redirect_url);
	setcookie(FRONT_COOKIE, "true", time() - 3600, '/');
	wp_safe_redirect($redirect_url);
	die();
}

// clears the login state:
function wpoa_clear_login_state() {
	unset($_SESSION ["WPOA"] ["USER_ID"]);
	unset($_SESSION ["WPOA"] ["USER_USERNAME"]);
	unset($_SESSION ["WPOA"] ["USER_NAME"]);
	unset($_SESSION ["WPOA"] ["USER_SURNAME"]);
	unset($_SESSION ["WPOA"] ["USER_EMAIL"]);
	unset($_SESSION ["WPOA"] ["USER_ROLENAMES"]);
	unset($_SESSION ["WPOA"] ["ACCESS_TOKEN"]);
	unset($_SESSION ["WPOA"] ["EXPIRES_IN"]);
	unset($_SESSION ["WPOA"] ["EXPIRES_AT"]);
	// unset($_SESSION["WPOA"]["LAST_URL"]);
}
function wpoa_get_user_roles($backofficeRolenames) {
	$roles = array();
	foreach ($backofficeRolenames as $each) {
		array_push($roles, wpoa_get_user_role($each));
	}
	return $roles;
}

function wpoa_get_user_role($backofficeRolename) {
	$role = ltrim($backofficeRolename, 'ROLE_');
	if ($role == 'admin') {
		$role = 'administrator';
	}
	return $role;
}

// login (or register and login) a wordpress user based on their oauth identity:
function wpoa_login_user($oauth_identity) {
	// store the user info in the user session so we can grab it later if we need to register the user:
	$_SESSION ["WPOA"] ["USER_ID"] = $oauth_identity ["id"];
	$_SESSION ["WPOA"] ["USER_USERNAME"] = $oauth_identity ["username"];
	$_SESSION ["WPOA"] ["USER_NAME"] = $oauth_identity ["name"];
	$_SESSION ["WPOA"] ["USER_SURNAME"] = $oauth_identity ["surname"];
	$_SESSION ["WPOA"] ["USER_EMAIL"] = $oauth_identity ["email"];
	$_SESSION ["WPOA"] ["USER_ROLENAMES"] = $oauth_identity ["roleNames"];
	// try to find a matching wordpress user for the now-authenticated user's oauth identity:
	$matched_user = wpoa_match_wordpress_user($oauth_identity);
	// handle the matched user if there is one:
	if ($matched_user) {
		// there was a matching wordpress user account, log it in now:
		$user_id = $matched_user->ID;
		$user_login = $matched_user->user_login;

		// update user roles.
		wpoa_update_user_roles($matched_user, $oauth_identity ["roleNames"]);

		wp_set_current_user($user_id, $user_login);
		wp_set_auth_cookie($user_id);
		do_action('wp_login', $user_login, $matched_user);
		// after login, redirect to the user's last location
		wpoa_end_login("Logged in successfully!");
	}
	// handle the already logged in user if there is one:
	if (is_user_logged_in()) {
		// there was a wordpress user logged in, but it is not associated with the now-authenticated user's email address, so associate it now:
		global $current_user;
		get_currentuserinfo();
		$user_id = $current_user->ID;
		wpoa_link_account($user_id);
		// after linking the account, redirect user to their last url
		wpoa_end_login("Your account was linked successfully with your third party authentication provider.");
	}
	// handle the logged out user or no matching user (register the user):
	if (! is_user_logged_in() && ! $matched_user) {
		// this person is not logged into a wordpress account and has no third party authentications registered, so proceed to register the wordpress user:
		include 'register.php';
	}
	// we shouldn't be here, but just in case...
	wpoa_end_login("Sorry, we couldn't log you in. The login flow terminated in an unexpected way. Please notify the admin or try again later.");
}

function wpoa_update_user_roles($user, $backofficeRolenames) {
	$roles = wpoa_get_user_roles($backofficeRolenames);
	$rolesToAdd = array_diff($roles, $user->roles);
	$rolesToRemove = array_diff($user->roles, $roles);
	foreach ($rolesToAdd as $eachRole) {
		$user->add_role($eachRole);
	}
	foreach ($rolesToRemove as $eachRole) {
		$user->remove_role($eachRole);
	}
}

// match the oauth identity to an existing wordpress user account:
function wpoa_match_wordpress_user($oauth_identity) {
	// attempt to get a wordpress user id from the database that matches the $oauth_identity['id'] value:
// 	global $wpdb;
// 	$usermeta_table = $wpdb->usermeta;
// 	$query_string = "SELECT $usermeta_table.user_id FROM $usermeta_table WHERE $usermeta_table.meta_key = 'wpoa_identity' AND $usermeta_table.meta_value LIKE '%" . $oauth_identity['provider'] . "|" . $oauth_identity['id'] . "%'";
// 	$query_result = $wpdb->get_var($query_string);
	// attempt to get a wordpress user with the matched id:
	$user = get_user_by('login', $oauth_identity['username']);
	return $user;
}

// links a third-party account to an existing wordpress user account:
function wpoa_link_account($user_id) {
	if ($_SESSION ['WPOA'] ['USER_ID'] != '') {
		add_user_meta($user_id, 'wpoa_identity', $_SESSION ['WPOA'] ['PROVIDER'] . '|' . $_SESSION ['WPOA'] ['USER_ID'] . '|' . time());
	}
}

