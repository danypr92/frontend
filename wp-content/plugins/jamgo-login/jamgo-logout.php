<?php

// start the user session for maintaining individual user states during the multi-stage authentication flow:
session_start();

// DEFINE THE OAUTH PROVIDER AND SETTINGS TO USE #
$_SESSION ['WPOA'] ['PROVIDER'] = 'Jamgo';

// define('URL_TOKEN', $baseBackInternalUrl . "/oauth/token");

wpoa_logout_user();

// logout the wordpress user:
// TODO: this is usually called from a custom logout button, but we could have the button call /wp-logout.php?action=logout for more consistency...
function wpoa_logout_user() {

	// logout the user:
	setcookie("JSESSIONID", "true", time() - 3600, '/');
	$user = null; 		// nullify the user
	session_destroy(); 	// destroy the php user session
	wp_logout(); 		// logout the wordpress user...this gets hooked and diverted to wpoa_end_logout() for final handling
}