<?php

function user_single($pamapamUser) {
	global $baseApiUrl;

	$userImageUrl = $pamapamUser->pictureUrl ? $baseApiUrl . $pamapamUser->pictureUrl : get_template_directory_uri().'/assets/images/xinxeta.png.png';
    $classImageUrl = $pamapamUser->pictureUrl ? 'xinxeta100' : 'xinxetaWhite';

    include 'user-details.php';
}

function published_entities($userId) {
	global $baseApiInternalUrl;
	global $baseApiUrl;

	$data = array(
			'page' => 0,
			'size' => 6,
			'userId' => $userId,
	);
	$jsonData = json_encode($data);
	$postArray = array(
			'headers' => array('Content-Type' => 'application/json'),
			'body' => $jsonData
	);
	$postUrl = $baseApiInternalUrl . "/searchEntities";

	$entities_response = wp_remote_post($postUrl, $postArray);
	$entities_array = json_decode(wp_remote_retrieve_body($entities_response));

	foreach ($entities_array->response->content as $entity) {
		include 'entity-box.php';
	}
}
