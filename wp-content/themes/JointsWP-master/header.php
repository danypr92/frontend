<!doctype html>
<!--comentario prueba git -->
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<!-- Force IE to use the latest rendering engine available -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta class="foundation-mq">
<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
<meta name="msapplication-TileColor" content="#f01d4f">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57808927-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-57808927-1');
</script>
<!-- end analytics -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- filtres voluntaris xinxetes con MixItUp-->
<!--<script src="https://demos.kunkalabs.com/mixitup/mixitup.min.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup-pagination.min.js"></script>
<!-- fin filtres voluntaris xinxetes -->
<!-- multifiltre per a punts -->
<!-- Requiere /js/mixitup/mixitup.min.js cargado más arriba. Alerta, no cambiar el orden -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup-multifilter.min.js"></script>
<!-- fin paginar punts -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dotdotdot/jquery.dotdotdot.min.js"></script>
</head>
<!-- Uncomment this line if using the Off-Canvas Menu -->
<body <?php body_class(); ?>>
	<div class="off-canvas-wrapper">

			<?php get_template_part( 'parts/content', 'offcanvas' ); ?>

			<div class="off-canvas-content" data-off-canvas-content>
			<header class="header" role="banner">
				<!-- <div id="idiomas" class="idiomas float-right">
						<?php pll_the_languages( array( 'dropdown' => 1 ) ); ?>
            <span class="icon-arrow-down"></span>
					</div> -->
				<div id="sessio" class="sessio float-right">
					<?php if (!is_user_logged_in()) { ?>
						<a href="<?php echo wp_login_url(); ?>">Inici sessió <span class="icon-arrow-right"></span></a>
					<?php } else { ?>
						<a href="<?php echo wp_logout_url("/"); ?>">Tancar sessió <span class="icon-arrow-right"></span></a>
					<?php } ?>
				</div>
				<div id="logo" class="clearfix">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pamapam-logo-bn.png" alt="Logo Pam a Pam" /></a>
				</div>
				<div id="lema">
						<?php print_r(get_bloginfo(description)); ?>
					</div>
				<!-- This navs will be applied to the topbar, above all content
						  To see additional nav styles, visit the /parts directory -->
					 <?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>

				</header>
			<!-- end .header -->
