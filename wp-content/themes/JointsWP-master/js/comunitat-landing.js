function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}

function getSendData(page, size) {
	var sendData = {
		action : "user_grid",
		page : page,
		size : size
	}

	var name = $('#searchName').val();
	if (name) {
		sendData.name = name;
	}

	var selectedTerritory = $('#searchTerritory option:selected').val();
	if (selectedTerritory) {
		var territoryParts = selectedTerritory.split('-');
		sendData.territoryId = territoryParts[0];
		sendData.territoryType = territoryParts[1];
	}

	var selectedCommunity = $('#searchCommunity option:selected').val();
	if (selectedCommunity) {
		sendData.communityId = selectedCommunity;
	}

	if (name || selectedTerritory || selectedCommunity) {
		$('#reset').css('display', 'inline-block');
	} else {

		$('#reset').css('display', 'none');

	}

	return sendData;
}

function moreContents(page, size) {
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			$('#entities-next-page').replaceWith(data);
			updateDotdotdot();
		}
	})
};


function setContents(page, size) {
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			$("#users").empty();
			$('#users').append(data);
			updateDotdotdot();
			$('#users .featured-image a img, #users a').hover(function() {
    			var outWidget = $(this).parents('.panel');
    			$(outWidget).addClass("visualEffect");
  			});
  			$('#users .featured-image a img, #users a').mouseout(function() {
    			var outWidget2 = $(this).parents('.panel');
    			$(outWidget2).removeClass("visualEffect");
  			});
			$(document).foundation();
		}
	})
};

var updateContents = true;

jQuery(document).ready(function() {


	$("#searchUsersForm").submit(function(event) {
		event.preventDefault();
		$("#users").empty();
		setContents();
	});

	$("#searchUsersForm").submit();

	$("#searchTerritory").SumoSelect({
		placeholder : 'Territoris',
		search : true
	});

	$("#searchTerritory").change(function() {
		if (updateContents) {
			$("#users").empty();
			setContents();
		}
	})

	$("#searchCommunity").SumoSelect({
		placeholder : 'Comunitats',
		search : true
	});

	$("#searchCommunity").change(function() {
		if (updateContents) {
			$("#users").empty();
			setContents();
		}
	})

	// cuando se hace reset desaparece el botón
	$("#reset").click(function() {
		$('#reset').css('display', 'inline-block');
		$('#searchName').val("");
		updateContents = false;
		$("#searchTerritory")[0].sumo.selectItem(0);
		$("#searchCommunity")[0].sumo.selectItem(0);
		updateContents = true;
		$("#users").empty();
		setContents();
	});

	updateDotdotdot();
	$("section.sectors > p").dotdotdot({
		height : 65,
		fallbackToLetter : true,
		watch : true
	});

    $('.xinxeta article a img, .xinxeta article h3.title a').hover(function() {
  		var outWidget = $(this).parents('.panel.xinxeta');
  		$(outWidget).addClass("visualEffect");
  	});
  	$('.xinxeta article a img, .xinxeta article h3.title a').mouseout(function() {
  		var outWidget2 = $(this).parents('.panel.xinxeta');
  		$(outWidget2).removeClass("visualEffect");
  	});

});
