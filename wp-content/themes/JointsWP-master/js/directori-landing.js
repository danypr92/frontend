function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}

function getSendData(page, size) {
	var sendData = {
		action : "entities_grid",
		page : page,
		size : size
	}

	var text = $('#searchText').val();
	if (text) {
		sendData.text = text;
	}

	var selectedSectors = [];
	$('#searchSectors option').each(function(eachSector) {
		if (this.selected == true) {
			selectedSectors.push(this.value);
		}
	});
	if (selectedSectors.length > 0) {
		sendData.sectorIds = selectedSectors;
	}

	var selectedTerritory = $('#searchTerritory option:selected').val();
	if (selectedTerritory) {
		var territoryParts = selectedTerritory.split('-');
		sendData.territoryId = territoryParts[0];
		sendData.territoryType = territoryParts[1];
	}

	if (text || selectedSectors.length > 0 || selectedTerritory) {
		$('#reset').css('display', 'inline-block');
	}

	return sendData;
}

function moreContents(page, size) {
	var url = $('#searchEntitiesForm').attr("action");
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			$('#entities-next-page').replaceWith(data);
			updateDotdotdot();
		}
	})
};

function setContents(page, size) {
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			$('#punts').append(data);
			updateDotdotdot();
			$('#punts .featured-image a img, #punts a').hover(function() {
    			var outWidget = $(this).parents('.panel');
    			$(outWidget).addClass("visualEffect");
  			});
  			$('#punts .featured-image a img, #punts a').mouseout(function() {
    			var outWidget2 = $(this).parents('.panel');
    			$(outWidget2).removeClass("visualEffect");
  			});
  			var article = getUrlParameter("article");
  			if (article !== undefined) {
	  			$('html, body').scrollTop($("#post-"+article).offset().top - 200);
  			}	
		}
	});
}

function replaceLocation(page, articleId){
	old_location = window.location.href;
	new_location = updateQueryStringParameter(old_location, 'article', articleId);
	new_location = updateQueryStringParameter(new_location, 'pag', page);
	window.history.pushState({}, '', new_location);
}

function updateQueryStringParameter(uri, key, value){
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)){
		return uri.replace(re, '$1' + key + "=" + value + "$2");
	} else {
		return uri + separator + key + "=" + value;
	}
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

jQuery(document).ready(function() {

	$("#searchEntitiesForm").submit(function(event) {
		event.preventDefault();
		$("#punts").empty();
		var old_page = getUrlParameter("pag");
		
		if (old_page !== undefined){

			var size = (parseInt(old_page) + 1) * 12;
			setContents(0, size);
		} else {
			setContents();
		}
	});

	$("#searchEntitiesForm").submit();

	$("#searchTerritory").SumoSelect({
		placeholder : 'Territoris',
		search : true
	});

	$("#searchTerritory").change(function() {
		$("#punts").empty();
		setContents();
	})

	$("#searchSectors").SumoSelect({
		placeholder : 'Tots els sectors'
	});

	$("#searchSectors").change(function() {
		$("#punts").empty();
		setContents();
	})

	// cuando se hace reset desaparece el botón
	$("#reset").click(function() {
		$('#reset').css('display', 'none');
		$('#searchText').val("");
		$("#searchTerritory")[0].sumo.unSelectAll();
		$("#searchSectors")[0].sumo.unSelectAll();
	});

	updateDotdotdot();
	$("section.sectors > p").dotdotdot({
		height : 65,
		fallbackToLetter : true,
		watch : true
	});

});
