if (!window.vaadin)
	alert("Failed to load the bootstrap JavaScript: " + "VAADIN/vaadinBootstrap.js");

/* The UI Configuration */
vaadin.initApplication("pamapam-backoffice", {
	"browserDetailsUrl" : php_vars.browserDetailsUrl,
	"serviceUrl" : php_vars.baseBackUrl + "/ui",
	"theme": "demoTheme",
	"versionInfo": {
	    "vaadinVersion": "8.0.5",
	    "atmosphereVersion": "2.4.5.vaadin2"
	},
	"widgetset": "org.pamapam.backoffice.Widgetset",
	"widgetsetReady": true,
	"vaadinDir" : php_vars.baseBackUrl + "/VAADIN/",
	"heartbeatInterval" : 300,
	"debug" : true,
	"standalone" : false,
	"authErrMsg" : {
		"message" : "Take note of any unsaved data, " + "and <u>click here<\/u> to continue.",
		"caption" : "Authentication problem"
	},
	"comErrMsg" : {
		"message" : "Take note of any unsaved data, " + "and <u>click here<\/u> to continue.",
		"caption" : "Communication problem"
	},
	"sessExpMsg" : {
		"message" : "Take note of any unsaved data, " + "and <u>click here<\/u> to continue.",
		"caption" : "Session Expired"
	}
});
