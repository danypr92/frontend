<?php /* Template Name: AFEGEIX INICIATIVA */ ?>

<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<div class="box-head">
	      <h1 class="page-title">Afegeix una iniciativa</h1>
          <?php echo addVesAlMapa(); ?>
	    </div>


		    <main id="main" class="large-12 small-12 columns participa" role="main">

			<!-- bloc 01 -->
				<div class="row out">
					<div class="celito left purple"></div>
					<div class="celito right purple"></div>

						<div class="large-6 small-12 columns">
							<a href="<?php echo get_site_url(); ?>/proposa-punt/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/08_proposa_iniciativa.JPG" alt="Pamapam" /></a>
							<h3><a href="<?php echo get_site_url(); ?>/proposa-punt/">Vols proposar una nova iniciativa?</a></h3>
						</div>
						<div class="large-6 small-12 columns">
							<p><a href="<?php echo get_site_url(); ?>/proposa-punt/">Si no ets usuàri/a ni voluntàri/a de Pam a Pam però coneixes alguna
iniciativa que creus que podria estar al mapa, proposa-la!</a></p>
							<a href="<?php echo get_site_url(); ?>/proposa-punt/"><span class="icon-arrow-right"></span></a>
						</div>
				</div> <!-- END bloc 01 -->

				<!-- bloc 02 -->
					<div class="row out">
						<div class="celito left green"></div>
						<div class="celito right green"></div>
							<div class="large-6 small-12 columns">
								<a href="<?php echo get_site_url(); ?>/iniciativa/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/09_iniciativa.JPG" alt="Pamapam" /></a>
								<h3><a href="<?php echo get_site_url(); ?>/iniciativa/">Ets una nova iniciativa de l'ESS?</a></h3>
							</div>
							<div class="large-6 small-12 columns">
								<p><a href="<?php echo get_site_url(); ?>/iniciativa/">Voleu aparéixer al mapa de Pam a Pam? Ompliu la fitxa bàsica i
ben aviat una voluntària es posarà en contacte amb vosaltres!</a></p>
								<a href="<?php echo get_site_url(); ?>/iniciativa/"><span class="icon-arrow-right"></span></a>
							</div>
					</div> <!-- END bloc 02 -->

					<!-- bloc 03 -->
						<?php
        				if (is_user_logged_in()) {
						echo '<div class="row out">';
						echo '<div class="celito left orange"></div>';
						echo '<div class="celito right purple"></div>';
						echo '<div class="large-6 small-12 columns">';
						echo '<a href="' . get_site_url() . '/wp-admin/admin.php?page=pamapam-new-entity"><img src="'. get_template_directory_uri() .'/assets/images/10_login.JPG" alt="Pamapam" /></a>';
						echo '<h3><a href="' . get_site_url() . '/wp-admin/admin.php?page=pamapam-new-entity">Ja ets Xinxeta?</a></h3>';
						echo '</div>';
						echo '<div class="large-6 small-12 columns">';
						echo '<p><a href="' . get_site_url() . '/wp-admin/admin.php?page=pamapam-new-entity">Has entrevistat una iniciativa i vols omplir-ne la fitxa? Inicia sessió!</a></p>';
						echo '<a href="' . get_site_url() . '/wp-admin/admin.php?page=pamapam-new-entity"><span class="icon-arrow-right"></span></a>';
						echo  '</div>';
						echo  '</div>';

					} else {

						echo '<div class="row out">';
						echo '<div class="celito left orange"></div>';
						echo '<div class="celito right purple"></div>';
						echo '<div class="large-6 small-12 columns">';
						echo '<a href="' . get_site_url() . '/backoffice/ui/login"><img src="' . get_template_directory_uri() . '/assets/images/10_login.JPG" alt="Pamapam"></a>';
						echo '<h3><a href="'. get_site_url() . '/backoffice/ui/login">Ja ets Xinxeta?</a></h3>';
						echo '</div>';
						echo '<div class="large-6 small-12 columns">';
						echo '<p><a href="' . get_site_url() . '">Has entrevistat una iniciativa i vols omplir-ne la fitxa? Inicia sessió!</a></p>';
						echo '<a href="' . get_site_url() . '"><span class="icon-arrow-right"></span></a>';
						echo  '</div>';
						echo  '</div>';

					}

					?>
