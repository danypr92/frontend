<?php
/*
 * Template Name: Backoffice
 */
?>

<?php get_header('vaadin'); ?>
<div id="content" class="vaadin-content">
	<div id="inner-content" class="row vaadin-inner-content">
		<main id="main" class="large-12 medium-12 columns vaadin-main" role="main">
		<div style="width: 100%; border: 2px solid green;" id="pamapam-backoffice" class="v-app">
			<div class="v-app-loading"></div>
			<noscript>[replaceable]You have to enable javascript in your browser to use an application built with Vaadin.</noscript>
		</div>
		</main>
	</div>
</div>
<?php get_footer(); ?>
