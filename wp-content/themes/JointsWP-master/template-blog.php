<?php /* Template Name: BLOG */ ?>

<!--Query Post-->
<?php
 $queryPost = array( 'post_type'=> 'post',
     'post_status'=> 'publish',
     'posts_per_page'=> '10',
     'paged' => get_query_var('paged'))
?>

<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<div class="box-head">
				 <h1 class="page-title">Blog</h1>
                <?php echo addVesAlMapa(); ?>
			</div>

		    <main id="main" class="large-12 medium-6 columns blog" role="main">
                <?php query_posts($queryPost);?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="large-6 medium-6 columns ">
                        <?php get_template_part( 'parts/loop', 'archive' ); ?>
                    </div>
					<?php endwhile; ?>
                    <div class="large-12 medium-12 columns ">
                        <?php joints_page_navi(); ?>
                    </div>
			    <?php endif; wp_reset_query(); ?>
			</main> <!-- end #main -->
		    <?php get_sidebar(); ?>
		</div> <!-- end #inner-content -->
	</div> <!-- end #content -->

	<div class="contain-to-grid sand-bkg">
	  <?php get_template_part( 'parts/include', 'afiliation' ); ?>
	   <?php get_template_part( 'parts/include', 'footer-socialwalls' ); ?>
	</div>

<?php get_footer(); ?>
