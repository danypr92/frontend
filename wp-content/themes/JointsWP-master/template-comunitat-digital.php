<?php /* Template Name: Comunitat Digital */ ?>

<?php get_header(); ?>
<div class="row upper-mar">
<div class="box-head">
  <h1 class="page-title"><?php the_title(); ?></h1>
  <?php echo addVesAlMapa(); ?>
</div>
</div>

<div class="ima-text"><img src="<?php echo get_home_url(); ?>/../wp-content/uploads/2018/07/cuquipula_colorified.jpg"/>


<div class="txt-encima-ima">

<div class="row">
  <div class="large-6 medium-6 small-12 columns txt-destacado"><?php the_field('foto_text_esquerra'); ?></div>
  <div class="large-6 medium-6 small-12 columns parrafo-foto"><?php the_field('foto_text_dreta'); ?></div>
</div>
</div>

</div>
<div id="content">

		<div id="inner-content" class="row">

<div class="large-12 columns">
  <p class="parrafo-g"><?php the_field('text_a_sota_de_la_foto'); ?></p>
</div>



<div class="large-12 medium-12 small-12 columns back-green">
<div class="large-6 medium-6 small-12 columns col-izq">
  <h4 class="txt-grey"><?php the_field('titol_llista'); ?></h4>
  <ul><?php the_field('llista'); ?></ul>
</div>
<div class="large-6 medium-6 small-12 columns col-dcha">
  <?php the_field('text_a_sota_de_la_llista'); ?>

</div>


    </div>


    <div class="large-12 medium-12 small-12 columns block-mercat">
      <div class="large-6 medium-6 small-12 columns mercat-left"><?php the_field('titol_mercat_social'); ?></div>
      <div class="large-6 medium-6 small-12 columns mercat-right"><?php the_field('text_mercat_social'); ?></div>

    </div>


<div class="large-12 medium-12 small-12 columns back-orange">
  <div class="large-6 medium-6 small-12 columns iniciatives-left">
    <h3 class="txt-grey">Notícies de la comunitat digital</h3>
    <p><!--<?php the_field('text_iniciatives'); ?>--></p>
  </div>
    <main id="main" class="large-12 medium-12 columns blog" role="main">
                <!--<?php query_posts($queryPost);?>-->
                <?php query_posts('posts_per_page=3'); ?>
                <?php query_posts('cat=388'); ?>
 
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="large-4 medium-3 columns ">
                        <?php get_template_part( 'parts/loop', 'archive' ); ?>
                    </div>
					<?php endwhile; ?>
                    <!-- enllaç a veure més noticies comentat -->
                    <!--
                    <div class="large-12 medium-12 columns ">
						<nav class="page-navigation">
                        	<a href="https://pamapam.org/ca/blog/category/comunitat-digital/">Veure més notícies de la comunitat digital</a>
						</nav>
                    </div>
                    -->
			    <?php endif; wp_reset_query(); ?>
			</main> <!-- end #main -->

</div>




  </div>

</div>

<?php get_footer(); ?>

