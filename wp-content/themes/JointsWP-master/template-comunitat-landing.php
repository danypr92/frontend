<?php
/*
Template Name: LANDING Comunitat
*/
?>

<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/comunitat-landing.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.js"></script>
<?php
function getCommunities() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/communities";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}
function getTerritories() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/territories";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$communities = getCommunities()->response;
$territories = getTerritories()->response;
// Adjust the amount of rows in the grid
$grid_columns = 4;
?>

<div id="content">
	<div id="inner-content" class="row">
		<main id="main" class="large-12 medium-12 columns" role="main">
			<div class="box-head">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php echo addVesAlMapa(); ?>
				<p>La comunitat de Pam a Pam és diversa i heterogènia, però totes les Xinxetes que la conformen alimenten el mapa i participen per fer créixer el projecte.</p>
			</div>

			<div class="box-search">
				<div class="celito left green"></div>
				<div class="celito right orange"></div>
				<form id="searchUsersForm">
					<input type="submit" style="display: none" />
					<div class="row">
						<div class="large-4 columns">
							<select id="searchTerritory">
								<option id="0"></option><?php
									foreach($territories as $eachTerritory) {
		                            	$territoryId = $eachTerritory->id . '-' . $eachTerritory->type; ?>
		                            	<option id="<?php echo $territoryId; ?>" value="<?php echo $territoryId; ?>"><?php echo $eachTerritory->name; ?></option>
		                            <?php } ?>
							</select>
						</div>
						<div class="large-4 columns">
							<select id="searchCommunity">
								<option id="0"></option><?php
									foreach ($communities as $eachCommunity) {?>
										<option id="<?php echo $eachCommunity->id; ?>" value="<?php echo $eachCommunity->id; ?>"><?php echo $eachCommunity->name; ?></option>
									<?php } ?>
							</select>
						</div>
						<div class="large-4 columns txt-search">
							<input id="searchName" type="text" name="name" value="" placeholder="Cerca per nom">
							<div>
								<div id="resultats"></div>
							</div>
						</div>

						<div class="large-12 columns">
							<div id="reset">
								<button type="reset" class="control control-text button">Tots<span class="icon-iconos-my-roca-comparer"></span>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- Grilla de voluntaris, no cambiar la clase -->
			<div id="users" class="container">
			</div>
		</main>
	</div>
</div>

<!-- banner gran -->
<div class="contain-to-grid">
	<div class="large-12 columns comunitat banner">
		<div>
			<h3>Comunitat</h3>
			<h4>Necessitem ser el màxim de persones per tot el territori per poder tenir el mapa més complert possible </h4>
			<p><a href="<?php echo get_home_url();?>/xinxeta/">Fes-te Xinxeta!</a></p>
			<a href="<?php echo get_home_url();?>/xinxeta/"><span class="icon-arrow-right"></span></a>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/11_banner_comunitat_horitzontal.jpg">
	</div>
</div>

<div class="contain-to-grid sand-bkg">
	<?php get_template_part( 'parts/include', 'afiliation' ); ?>
</div>
<?php get_footer(); ?>
