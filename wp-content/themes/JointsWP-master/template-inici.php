<?php /* Template Name: Inici No Sidebar */ ?>

<?php get_header(); ?>

	<div id="content">
		<!-- inicio mapa -->
		<!-- esto a hoja de estilos -->
			<style type="text/css">
			</style>
		<div id="map" class="contain-to-grid">
		</div>

		<div id='searchContainer' style="position: fixed;">

		    <script type="text/javascript">


		        var xarxes = L.icon({
		            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/xar.png',
		            /*
		            iconSize: [25, 41],
		            iconAnchor: [12, 41],
		            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
		            shadowSize: [41, 41],
		            shadowAnchor: [13, 41],
		            popupAnchor:  [0, -28]
		            */
		        });

		        var vestits = L.icon({
		            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ves.png',
		            /*
		            iconSize: [25, 41],
		            iconAnchor: [12, 41],
		            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
		            shadowSize: [41, 41],
		            shadowAnchor: [13, 41],
		            popupAnchor:  [0, -28]
		            */
		        });

		        var alimentacio = L.icon({
		            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ali.png',
		            /*
		            iconSize: [25, 41],
		            iconAnchor: [12, 41],
		            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
		            shadowSize: [41, 41],
		            shadowAnchor: [13, 41],
		            popupAnchor:  [0, -28]
		            */
		        });

		        var mapboxTiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
		            maxZoom: 18,
		            attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>'
		        });

		        var map = L.map('map')
		            .addLayer(mapboxTiles)
		            .setView([39.80, 34.00], 6); /* centrado en Turquía */

		            // Add layer for markercluster
		            var markers = L.markerClusterGroup();

		            // input filtros
		            var options = {
		                geojsonServiceAddress: "<?php echo get_template_directory_uri(); ?>/testgeojsondata.json"
		                };
		                $("#searchContainer").GeoJsonAutocomplete(options);

		    </script>
			</div>

		<!-- fin mapa -->

		<div id="inner-content" class="row">

		    <main id="main" class="large-12 medium-12 columns" role="main">

		<!-- box 01 -->
					<div class="large-12 medium-12 columns box-02 green">
						<div class="large-4 columns">
							<p>Consulta la llista</p>
						</div>
						<div class="large-8 columns">
							<p>Directori de Punts</p>
						</div>
					</div>

		<!-- box 02 -->
					<div class="large-12 medium-12 columns box-02">
						<div class="large-6 columns">
							<p>Una foto</p>
						</div>
						<div class="large-6 columns">
							<h2>El teu mapa del consum responsable</h2>
							<p>Un text</p>
						</div>
					</div>

		<!-- box 03 -->
				<div class="large-12 medium-12 columns box-03 purple">
						<div class="large-4 columns">
							<p>Què has de fer per sortir al mapa?</p>
						</div>
					<div class="large-8 columns">
							<p>Els 15 criteris</p>
					</div>
				</div>



			</main> <!-- end #main -->


		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
