<?php
/*
Template Name: Inici geojoson_markercluster (No Sidebar)
*/
?>

<?php get_header(); ?>
			
	<div id="content">
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">

<!-- inicio mapa -->
    <!-- esto a hoja de estilos -->
    <style type="text/css">
    #map {
        width: 100%;
        height: 400px;
    }
    </style>

<div id="map"></div>

    <script type="text/javascript">


        var xarxes = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/xar.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var vestits = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ves.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var alimentacio = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ali.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.org/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var mapboxTiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>'
        });

        var map = L.map('map')
            .addLayer(mapboxTiles)
            .setView([41.4, 1.7], 8); /* centrado en catalunya */

            // Add layer for markercluster
            var markers = L.markerClusterGroup();

            // Use jQuery to load date from GeoJSON file 
            $.getJSON('../../../mapa/punts.json', function(data) {
            var geoJsonLayer = L.geoJson(data, {

            // icono según sector
            pointToLayer: function(feature, latlng) {
                // ahora los sectores vienen por su número. Por ejemplo -12-24-32. Obtengo el primer número, substring
                sector = feature.properties.field_sector_subsectors.substring(1, 3);
                // según el número de sector hago case
                switch (sector) {
                    case "62":
                        return new L.Marker(latlng, {icon: xarxes});
                        break;
                    case "12":
                        return new L.Marker(latlng, {icon: alimentacio});
                        break;
                    default:
                        return new L.Marker(latlng, {icon: vestits});
                }
            },

    
            onEachFeature: function(feature, layer){
              // with popup info
              layer.bindPopup(feature.properties.title);
              // con esto cambiaríamos el icono para todos los puntos
              //layer.setIcon(xarxes);
            }

          });
          // Add geoJsonLayer to markercluster group
          markers.addLayer(geoJsonLayer);
          // Add the markercluster group to the map
          map.addLayer(markers);
        });


    </script>



<!-- fin mapa -->				
	<?php
		// TO SHOW THE PAGE CONTENTS
    		while ( have_posts() ) : the_post(); 
	?> <!--Because the_content() works only inside a WP Loop -->
        <div class="entry-content-page">
            <?php the_content(); ?> <!-- Page Content -->
        </div><!-- .entry-content-page -->
    	<?php
    		endwhile; //resetting the page loop
	?>

	<div id="homedinamica">
	        <div id="homeblog">
			aquí los dos posts
		</div>

		<div id="homexarxes">
			aquí les xarxes
		</div>
	</div>
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
