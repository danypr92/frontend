
<?php /* Template Name: GRACIES-XINXETA */ ?>


<?php get_header(); ?>
<div class="row">
    <div class="large-6 small-12 columns">
        <span class=link-to-inici><a href="<?php echo get_site_url();?>" class="button button-large"><i class="icon-arrow-left2 margin-right-5" ></i>Inici</a></span>
        <div class="row">
            <div class="large-8 small-8 large-centered small-centered columns">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" width="100%">
            </div>
            <div class="small-12 small-centered columns margin-top-15 ">
                <h4><?php
                                if (get_field('texto-ok-xinxeta')) {
                                    echo get_field('texto-ok-xinxeta');
                                }
                            ?></h4>
            </div>
        </div>
    </div>
    <div class="large-6 columns small-12 voluntaria-background" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/infografiapampam2.png');">
    </div>
</div>