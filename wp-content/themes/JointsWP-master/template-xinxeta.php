<?php /* Template Name: FES-TE XINXETA */ ?>


<?php 

get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>

<div class="row">
    <div class="large-6 small-12 columns padding-right-30" >
        <span class=link-to-inici><a href="<?php echo get_site_url();?>" class="button button-large"><i class="icon-arrow-left2 margin-right-5" ></i>Inici</a></span>
        <div class="row">
            <div class="large-8 small-8 large-centered small-centered columns">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" width="100%">
                </div>
                 <div class="small-12 small-centered columns margin-top-15">
                 <h4><?php
                                if (get_field('titol-xinxeta')) {
                                    echo get_field('titol-xinxeta');
                                }
                            ?></h4>
            </div>
            <div class="small-12 small-centered columns">
                <h5><?php
                                if (get_field('texto-xinxeta')) {
                                    echo get_field('texto-xinxeta');
                                }
                            ?></h5>
            </div>
        </div>
        <div class="row">
            <form action="" class="xinxeta-form" method="post">
                <div class="large-12 columns">
                    <p class="border-left-celito">El teu Nom</p>
                    <input type="text" name="name" class="input" required>
                </div>
                <div class="large-12 columns">
                    <p>El teu email</p>
                    <input type="mail" name="email" class="input" required>
                </div>
                <div class="small-8 small-centered columns">
                    <button type="submit" name="xinxeta-submit" class="button-submit" value="">
                        <span>Vull fer-me Xinxeta de Pam a Pam</span> <i class="icon icon-arrow-right "></i>
                    </button>
                </div>

            </form>
            <div class="small-12 small-centered columns">
                <h5>
<?php
					if (get_field('politica-dades')) {
						echo get_field('politica-dades');
					}
?>
				</h5>
            </div>
        </div>

    </div>

   <div class="large-6 columns small-12 voluntaria-background" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/infografiapampam2.png');">
    </div>

</div>

<script type="text/javascript">
    $(document).foundation();
</script>



